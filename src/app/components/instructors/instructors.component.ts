import { Component, OnInit } from '@angular/core';
import { InstructorsService } from '../../services/instructors.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-instructors',
  templateUrl: './instructors.component.html',
  styleUrls: ['./instructors.component.scss']
})

export class InstructorsComponent implements OnInit {
  public cols: any[];
  public instructionsList: any[];
  public firstName: string;
  public lastName: string;
  public addInstructorModal: boolean;
  public instructionId: number;

  constructor(
    private instructorsService: InstructorsService, 
    private messageService: MessageService
  ) {
    this.cols = [
      { field: 'firstName', header: 'First Name' },
      { field: 'lastName', header: 'Last Name' }
    ];
  }

  ngOnInit() {
    this.getInstructors();
  }

  public deleteInstruction(id: any) {
    this.instructionsList = this.instructionsList.filter((instruction) => {
      return instruction.id !== id;
    });
    this.instructorsService.deleteInstruction(id).subscribe((res) => {
      const message = {
        severity: 'success', 
        summary: 'Instructor successfully deleted!', 
      };

      this.messageService.add(message);
    });
  }

  public addInstruction() {
    this.addInstructorModal = true;
  }

  public addInstructor() {
    const lastInsctructionId = this.instructionsList.slice(-1).pop().id;
    this.instructionId = lastInsctructionId + 1;
    
    const instructor = {
      id: this.instructionId,
      firstName: this.firstName,
      lastName: this.lastName
    };

    this.instructionsList.push(instructor);

    this.instructorsService.createInstruction(instructor).subscribe((res) => {
      const message = {
        severity: 'success', 
        summary: 'Instructor successfully created!', 
      };

      this.messageService.add(message);
      this.clear();
    });
  }

  public clear() {
    this.addInstructorModal = false;
    this.firstName = null;
    this.lastName = null;
  }

  private getInstructors() {
    this.instructorsService.getInstructions().subscribe((res: any) => {
      this.instructionsList = res;
    });
  }
}

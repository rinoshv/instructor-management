import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class InstructorsService {
  constructor(private http: HttpClient) {}

  getInstructions() {
    let headers = new HttpHeaders();
    return this.http.get(`${environment.apiUrl}/instructors`, { headers });
  }

  createInstruction(body: object) {
    let headers = new HttpHeaders();

    return this.http.post(`${environment.apiUrl}/instructors`, body, { headers });
  }

  deleteInstruction(id: any) {
    return this.http.delete(`${environment.apiUrl}/instructors/${id}`);
  }
}

import { Routes, RouterModule } from '@angular/router';

import { InstructorsComponent } from './components/instructors/instructors.component';

const routes: Routes = [
  {
    path: 'instructors',
    component: InstructorsComponent
  },
  {
    path: '**',
    redirectTo: 'instructors'
  }
];

export const routing = RouterModule.forRoot(routes);